![jMail](jMail-logo.png)
##jMail是什么?
一款轻量级的开源邮件服务，采用mongodb数据库，支持web、smtp、pop3、imap


##jMail有哪些功能？

* `核心` —— 服务基础框架、配置管理器，邮件阵列服务
* `web` —— web呈现模块，用于web管理及客户收发邮件
* `stmp` —— 发送模块
* `pop` —— pop接受模块
* `imap` —— imap接受模块


##问题反馈
在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流

* QQ: 31325423
* Mail：[@Buddy-Deus](mailto:31325423@qq.com)


##捐助开发者
本项目源于自身的强迫症，希望你喜欢我的作品，同时也能支持一下。
当然，有钱捧个钱场（直接加我的QQ给我发红包就好了），没钱捧个人场，谢谢各位。


##感激
感谢以下的项目,排名不分先后

* [Express](http://expressjs.com)
* [jMail](http://jmail.cc)

