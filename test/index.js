"use strict";

const path = require("path"),
	  jBD  = require("jbd"),
	  Svr  = require("jbd"),
	  TEST = require("./module/TEST/index.js");

let cfg, core, app;

cfg = Svr.Config(
	path.join(__dirname, "./config.json"),
	{
		root:   path.join(__dirname, "./public"),
		api:    path.join(__dirname, "./api"),
		view:   path.join(__dirname, "./views"),
		// page:   path.join(__dirname, "./module/Page"),
		upload: path.join(__dirname, "./public/upload")
	},
	{
		DB:   jBD.DB,
		File: Svr.File,
		Act:  Svr.Act
	},
	(cfg, key, svr, file, opt, plugsin) => {
		switch (key) {
			case "member":
				svr[key] = file[key] = {
					db: cfg.key
				};
				break;
		}
	}
);

core = TEST(cfg.plugins, cfg.module);

app = Svr(cfg, core);