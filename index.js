var path   = require("path"),
	cfg    = require("./config.json"),
	global = require("./bin/global.js")(),
	bin    = path.join(__dirname, "bin"),
	out    = path.join(__dirname, "public");

for (var i = 0, p; i < cfg.project.length && (p = cfg.project[i]); i++) {
	console.log("===== " + p + " =====\n");
	require(path.join(bin, p + ".js"))(global, cfg.info, path.join(__dirname, p), path.join(out, p));
	console.log("============================\n");
}

console.log("All OK\n");

process.exit();