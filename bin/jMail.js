"use strict";

module.exports = exports = function (global, info, code, out) {
	const path  = require("path"),
		  cfg   = require(path.join(code, "config.json")),
		  cache = path.join(out, "../cache");

	global.delDir(path.join(out, "../../test/module/jMail"), true);

	const init_Cache = (code, out, cache) => {
		let list = {};

		global.delDir(out, false);
		//global.delFile(path.join(code, "jBD.js"));

		console.log("─┬─ cache");

		console.log(" ├─ jMail.js");
		global.miniFile(path.join(code, "index.js"), path.join(cache, "index.js"));

		console.log(" ├┬ lib");
		global.each(cfg.lib, (d, k, l) => {
			l = "lib/" + k + "/";

			console.log(" │├─ " + k);
			global.each(d, (d, o) => {
				o = l + d;
				list[o] = o;
				global.miniFile(path.join(code, o), path.join(cache, o));
				console.log(" ││  ├─ " + d);
			});
			console.log(" ││  └─ done\n ││");
		});
		console.log(" │└─ done\n │");

		// console.log(" ├┬ src");
		// global.each(cfg.src.files, (d, o) => {
		// 	o = path.join(code, d);
		// 	d = path.basename(o);
		// 	list.web["src/" + d] = "./src/" + d;
		// 	global.miniFile(o, path.join(cache, "src", d));
		// 	console.log(" │├─ " + o);
		// });
		// console.log(" │└─ done\n │");

		console.log(" └─ done\n");

		return list;
	};

	cfg.cache = init_Cache(code, out, cache);

	console.log("─┬─ node@" + cfg.version);
	cfg.pack = global.deep(cfg.info);
	cfg.pack = global.deep(info, cfg.pack);
	cfg.pack.version = cfg.version;
	cfg.pack.main = "index.js";
	cfg.pack.files = [];
	global.each(cfg.cache, (d, k) => {
		cfg.pack.files.push(k);
		global.copyFile(path.join(cache, d), path.join(out, d));
		console.log(" ├─ " + d);
	});
	global.save(path.join(out, "package.json"), cfg.pack);
	global.copyFile(path.join(cache, "index.js"), path.join(out, "index.js"));
	console.log(" └─ done\n");

	console.log("─┬─ test@" + cfg.version);
	global.copyDir(path.join(out), path.join(out, "../../test/module/jMail"));
	console.log(" └─ done\n");

	global.delDir(cache, true);
};